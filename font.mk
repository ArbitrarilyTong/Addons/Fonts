PRODUCT_PACKAGES := \
    MiSans-Bold.ttf \
    MiSans-Demibold.ttf \
    MiSans-ExtraLight.ttf \
    MiSans-Heavy.ttf \
    MiSans-Light.ttf \
    MiSans-Medium.ttf \
    MiSans-Normal.ttf \
    MiSans-Regular.ttf \
    MiSans-Semibold.ttf \
    MiSans-Thin.ttf